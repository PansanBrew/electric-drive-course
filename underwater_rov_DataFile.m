% Simscape(TM) Multibody(TM) version: 7.4

% This is a model data file derived from a Simscape Multibody Import XML file using the smimport function.
% The data in this file sets the block parameter values in an imported Simscape Multibody model.
% For more information on this file, see the smimport function help page in the Simscape Multibody documentation.
% You can modify numerical values, but avoid any other changes to this file.
% Do not add code to this file. Do not edit the physical units shown in comments.

%%%VariableName:smiData
Environment.fluid_density=1025;

Thruster.parameters.damping=0.001 % N*m/deg/s
Thruster.parameters.diameter=70/1000; % mm
%Thruster.parameters.KT=17.1946/(1025)/(70/1000)^4/(3000*pi/30)^2
%Thruster.parameters.KQ=0.2555/(1025)/(70/1000)^5/(3000*pi/30)^2
Thruster.parameters.KT=0.0071;
Thruster.parameters.KQ=0.0015;




%============= RigidTransform =============%

%Initialize the RigidTransform structure array by filling in null values.
smiData.RigidTransform(21).translation = [0.0 0.0 0.0];
smiData.RigidTransform(21).angle = 0.0;
smiData.RigidTransform(21).axis = [0.0 0.0 0.0];
smiData.RigidTransform(21).ID = '';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(1).translation = [29.500000000000011 0 0];  % mm
smiData.RigidTransform(1).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(1).axis = [-0.57735026918962584 -0.57735026918962584 0.57735026918962584];
smiData.RigidTransform(1).ID = 'B[thruster:screw-1:-:thruster:screw housing end-1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(2).translation = [4.5414186286701332e-14 9.4042491163486546e-14 1.9999999999999165];  % mm
smiData.RigidTransform(2).angle = 3.1415926535897931;  % rad
smiData.RigidTransform(2).axis = [-1 4.4862003972910331e-33 -7.5437530101761812e-17];
smiData.RigidTransform(2).ID = 'F[thruster:screw-1:-:thruster:screw housing end-1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(3).translation = [0 0 2.0000000000000018];  % mm
smiData.RigidTransform(3).angle = 3.1415926535897931;  % rad
smiData.RigidTransform(3).axis = [-1 -0 -0];
smiData.RigidTransform(3).ID = 'B[thruster:screw housing-1:-:thruster:screw-1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(4).translation = [-7.0000000000000018 1.5054624213917123e-13 -4.0856207306205761e-14];  % mm
smiData.RigidTransform(4).angle = 2.0943951023931957;  % rad
smiData.RigidTransform(4).axis = [0.57735026918962584 0.57735026918962584 0.57735026918962562];
smiData.RigidTransform(4).ID = 'F[thruster:screw housing-1:-:thruster:screw-1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(5).translation = [-103.0230052090422 0.030351659143165798 -142.66163982807097];  % mm
smiData.RigidTransform(5).angle = 0.41388451951819033;  % rad
smiData.RigidTransform(5).axis = [1.1525075995790947e-15 1 2.4196650874799561e-16];
smiData.RigidTransform(5).ID = 'B[rov_centered.step-3:-:thruster-5:screw housing-1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(6).translation = [-11.25535671466371 2.1316282072803006e-14 -17];  % mm
smiData.RigidTransform(6).angle = 2.0943951023931962;  % rad
smiData.RigidTransform(6).axis = [-0.57735026918962562 -0.57735026918962595 0.57735026918962562];
smiData.RigidTransform(6).ID = 'F[rov_centered.step-3:-:thruster-5:screw housing-1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(7).translation = [-102.89451314485761 0.030351659143165649 142.36911868714142];  % mm
smiData.RigidTransform(7).angle = 3.1415926535897927;  % rad
smiData.RigidTransform(7).axis = [0.9786637577741184 4.9716463057132379e-17 0.20546836549562034];
smiData.RigidTransform(7).ID = 'B[rov_centered.step-3:-:thruster-2:screw housing-1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(8).translation = [-11.574854490006594 -2.8421709430404007e-14 -17.000000000000071];  % mm
smiData.RigidTransform(8).angle = 2.0943951023931957;  % rad
smiData.RigidTransform(8).axis = [-0.57735026918962584 -0.57735026918962584 0.57735026918962562];
smiData.RigidTransform(8).ID = 'F[rov_centered.step-3:-:thruster-2:screw housing-1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(9).translation = [102.89451314485761 0.030351659143165649 142.36911868714142];  % mm
smiData.RigidTransform(9).angle = 3.1415926535897927;  % rad
smiData.RigidTransform(9).axis = [0.9786637577741184 -4.9716463057132379e-17 -0.20546836549562034];
smiData.RigidTransform(9).ID = 'B[rov_centered.step-3:-:thruster-3:screw housing-1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(10).translation = [-11.574854490006475 -8.8817841970012523e-15 -17.000000000000092];  % mm
smiData.RigidTransform(10).angle = 2.0943951023931957;  % rad
smiData.RigidTransform(10).axis = [-0.57735026918962595 -0.57735026918962595 0.57735026918962562];
smiData.RigidTransform(10).ID = 'F[rov_centered.step-3:-:thruster-3:screw housing-1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(11).translation = [103.0230052090422 0.030351659143165798 -142.66163982807097];  % mm
smiData.RigidTransform(11).angle = 0.41388451951819033;  % rad
smiData.RigidTransform(11).axis = [1.1525075995790947e-15 -1 -2.4196650874799561e-16];
smiData.RigidTransform(11).ID = 'B[rov_centered.step-3:-:thruster-4:screw housing-1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(12).translation = [-11.255356714663774 2.3092638912203256e-14 -16.999999999999968];  % mm
smiData.RigidTransform(12).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(12).axis = [-0.57735026918962584 -0.57735026918962584 0.57735026918962584];
smiData.RigidTransform(12).ID = 'F[rov_centered.step-3:-:thruster-4:screw housing-1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(13).translation = [0 0 0];  % mm
smiData.RigidTransform(13).angle = 1.5707963267949003;  % rad
smiData.RigidTransform(13).axis = [1 0 0];
smiData.RigidTransform(13).ID = 'RootGround[rov_centered.step-3]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(14).translation = [-79.429976484794935 -14.773556260763598 108.33275966600202];  % mm
smiData.RigidTransform(14).angle = 0;  % rad
smiData.RigidTransform(14).axis = [0 0 0];
smiData.RigidTransform(14).ID = 'AssemblyGround[thruster-2:screw housing-1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(15).translation = [-79.429976484795077 -14.773556260763835 75.832759666001905];  % mm
smiData.RigidTransform(15).angle = 3.1415926535897931;  % rad
smiData.RigidTransform(15).axis = [-0.42153847024901414 -0.90681051940310065 -1.0569643428774843e-16];
smiData.RigidTransform(15).ID = 'AssemblyGround[thruster-2:screw housing end-1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(16).translation = [-79.429976484794935 -14.773556260763598 108.33275966600206];  % mm
smiData.RigidTransform(16).angle = 0;  % rad
smiData.RigidTransform(16).axis = [0 0 0];
smiData.RigidTransform(16).ID = 'AssemblyGround[thruster-3:screw housing-1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(17).translation = [-79.429976484795077 -14.773556260763835 75.832759666001948];  % mm
smiData.RigidTransform(17).angle = 3.1415926535897931;  % rad
smiData.RigidTransform(17).axis = [-0.42153847024901392 -0.90681051940310065 -5.7340357157200982e-17];
smiData.RigidTransform(17).ID = 'AssemblyGround[thruster-3:screw housing end-1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(18).translation = [-79.429976484794878 -14.7735562607636 108.33275966600198];  % mm
smiData.RigidTransform(18).angle = 0;  % rad
smiData.RigidTransform(18).axis = [0 0 0];
smiData.RigidTransform(18).ID = 'AssemblyGround[thruster-4:screw housing-1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(19).translation = [-79.429976484795048 -14.773556260763835 75.832759666001891];  % mm
smiData.RigidTransform(19).angle = 3.1415926535897931;  % rad
smiData.RigidTransform(19).axis = [-0.42153847024901409 -0.90681051940310053 -8.8919212626400703e-17];
smiData.RigidTransform(19).ID = 'AssemblyGround[thruster-4:screw housing end-1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(20).translation = [-79.429976484794878 -14.773556260763598 108.33275966600199];  % mm
smiData.RigidTransform(20).angle = 0;  % rad
smiData.RigidTransform(20).axis = [0 0 0];
smiData.RigidTransform(20).ID = 'AssemblyGround[thruster-5:screw housing-1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(21).translation = [-79.429976484794992 -14.773556260763836 75.832759666001891];  % mm
smiData.RigidTransform(21).angle = 3.1415926535897931;  % rad
smiData.RigidTransform(21).axis = [-0.42153847024901409 -0.90681051940310053 -1.3323717043147646e-16];
smiData.RigidTransform(21).ID = 'AssemblyGround[thruster-5:screw housing end-1]';


%============= Solid =============%
%Center of Mass (CoM) %Moments of Inertia (MoI) %Product of Inertia (PoI)

%Initialize the Solid structure array by filling in null values.
smiData.Solid(4).mass = 0.0;
smiData.Solid(4).CoM = [0.0 0.0 0.0];
smiData.Solid(4).MoI = [0.0 0.0 0.0];
smiData.Solid(4).PoI = [0.0 0.0 0.0];
smiData.Solid(4).color = [0.0 0.0 0.0];
smiData.Solid(4).opacity = 0.0;
smiData.Solid(4).ID = '';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(1).mass = 1.1983809332246396;  % kg
smiData.Solid(1).CoM = [-0.00013273316787824047 -5.3749149948946817e-05 -0.00015939913128994979];  % mm
smiData.Solid(1).MoI = [6257.999873295249 13572.546657508074 9462.3541021830315];  % kg*mm^2
smiData.Solid(1).PoI = [0.024513101787649837 -0.068752880604738165 -0.0017419377913594598];  % kg*mm^2
smiData.Solid(1).color = [0.50196078431372548 0 1];
smiData.Solid(1).opacity = 1;
smiData.Solid(1).ID = 'rov_centered.step*:*Default';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(2).mass = 0.033002551486400723;  % kg
smiData.Solid(2).CoM = [-0.69801548188182294 2.70648805564334e-06 -9.7167040785445646];  % mm
smiData.Solid(2).MoI = [26.47752189683014 27.392390972336667 32.077391090410053];  % kg*mm^2
smiData.Solid(2).PoI = [-9.6779045630961347e-06 -0.16781748107798741 -2.4880310978002906e-05];  % kg*mm^2
smiData.Solid(2).color = [0.50196078431372548 0 1];
smiData.Solid(2).opacity = 1;
smiData.Solid(2).ID = 'screw housing*:*Default';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(3).mass = 0.020483274668337572;  % kg
smiData.Solid(3).CoM = [11.340139362709023 0 0];  % mm
smiData.Solid(3).MoI = [3.8551617531715854 3.4742696362574232 3.474269636257389];  % kg*mm^2
smiData.Solid(3).PoI = [0 0 0];  % kg*mm^2
smiData.Solid(3).color = [0.10588235294117647 0.93725490196078431 0];
smiData.Solid(3).opacity = 1;
smiData.Solid(3).ID = 'screw*:*Default';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(4).mass = 0.035253330751530969;  % kg
smiData.Solid(4).CoM = [0 -3.0397668863984723e-10 8.869702460142598];  % mm
smiData.Solid(4).MoI = [2.4956252869308733 2.4956252870652418 2.861554288398557];  % kg*mm^2
smiData.Solid(4).PoI = [0 0 0];  % kg*mm^2
smiData.Solid(4).color = [0.50196078431372548 0 1];
smiData.Solid(4).opacity = 1;
smiData.Solid(4).ID = 'screw housing end*:*Default';


%============= Joint =============%
%X Revolute Primitive (Rx) %Y Revolute Primitive (Ry) %Z Revolute Primitive (Rz)
%X Prismatic Primitive (Px) %Y Prismatic Primitive (Py) %Z Prismatic Primitive (Pz) %Spherical Primitive (S)
%Constant Velocity Primitive (CV) %Lead Screw Primitive (LS)
%Position Target (Pos)

%Initialize the RevoluteJoint structure array by filling in null values.
smiData.RevoluteJoint(12).Rz.Pos = 0.0;
smiData.RevoluteJoint(12).ID = '';

smiData.RevoluteJoint(1).Rz.Pos = 76.046781446867698;  % deg
smiData.RevoluteJoint(1).ID = '[thruster-2:screw-1:-:thruster-2:screw housing end-1]';

%This joint has been chosen as a cut joint. Simscape Multibody treats cut joints as algebraic constraints to solve closed kinematic loops. The imported model does not use the state target data for this joint.
smiData.RevoluteJoint(2).Rz.Pos = -54.089707137895317;  % deg
smiData.RevoluteJoint(2).ID = '[thruster-2:screw housing-1:-:thruster-2:screw-1]';

smiData.RevoluteJoint(3).Rz.Pos = 76.046781446867726;  % deg
smiData.RevoluteJoint(3).ID = '[thruster-3:screw-1:-:thruster-3:screw housing end-1]';

%This joint has been chosen as a cut joint. Simscape Multibody treats cut joints as algebraic constraints to solve closed kinematic loops. The imported model does not use the state target data for this joint.
smiData.RevoluteJoint(4).Rz.Pos = -54.089707137895296;  % deg
smiData.RevoluteJoint(4).ID = '[thruster-3:screw housing-1:-:thruster-3:screw-1]';

smiData.RevoluteJoint(5).Rz.Pos = 76.046781446867698;  % deg
smiData.RevoluteJoint(5).ID = '[thruster-4:screw-1:-:thruster-4:screw housing end-1]';

%This joint has been chosen as a cut joint. Simscape Multibody treats cut joints as algebraic constraints to solve closed kinematic loops. The imported model does not use the state target data for this joint.
smiData.RevoluteJoint(6).Rz.Pos = -54.089707137895296;  % deg
smiData.RevoluteJoint(6).ID = '[thruster-4:screw housing-1:-:thruster-4:screw-1]';

smiData.RevoluteJoint(7).Rz.Pos = 76.046781446867698;  % deg
smiData.RevoluteJoint(7).ID = '[thruster-5:screw-1:-:thruster-5:screw housing end-1]';

%This joint has been chosen as a cut joint. Simscape Multibody treats cut joints as algebraic constraints to solve closed kinematic loops. The imported model does not use the state target data for this joint.
smiData.RevoluteJoint(8).Rz.Pos = -54.089707137895296;  % deg
smiData.RevoluteJoint(8).ID = '[thruster-5:screw housing-1:-:thruster-5:screw-1]';

smiData.RevoluteJoint(9).Rz.Pos = 90.000000000000256;  % deg
smiData.RevoluteJoint(9).ID = '[rov_centered.step-3:-:thruster-5:screw housing-1]';

smiData.RevoluteJoint(10).Rz.Pos = 90.000000000000099;  % deg
smiData.RevoluteJoint(10).ID = '[rov_centered.step-3:-:thruster-2:screw housing-1]';

smiData.RevoluteJoint(11).Rz.Pos = 90.000000000000114;  % deg
smiData.RevoluteJoint(11).ID = '[rov_centered.step-3:-:thruster-3:screw housing-1]';

smiData.RevoluteJoint(12).Rz.Pos = 90.000000000000242;  % deg
smiData.RevoluteJoint(12).ID = '[rov_centered.step-3:-:thruster-4:screw housing-1]';

