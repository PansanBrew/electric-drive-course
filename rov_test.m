%% ЛР №2
% Запустите проект
open("Eldrive_lab2.prj");

% загрузите исходные данные 
load("var_30.mat");

%% Задание про механические характеристики  (Задание 1)
%  Делаем своими ручками

%% Задание на динамические характеристики (Задание 2)
%  открываем файл с гребным винтом
open('thruster_dynamic.slx');
% запускаем и смотрим на графики

%% Задание на систему управления для ДПТ (Задание 3)
% открываем файл с динамикой робота
open('underwater_rov.slx');
% Система позиционирования роботов уже сделана за вас

% необходимо настроить коэффициенты регулятора по скорости
% для гребных винтов.

%  На вход дпт подается желаемая скорость

% Логика винтов реализована в блоках
% thruster_№ 

%% 

% Всякие регуляторы
PID_surge.Kp=6
PID_surge.Ki=0;
PID_surge.Kd=45;

% Y direction sway PID
PID_sway.Kp=7;
PID_sway.Ki=0;
PID_sway.Kd=45;

% Z rotation yaw PID
PID_yaw.Kp=6;
PID_yaw.Ki=0.0001;
PID_yaw.Kd=0.8;


Kp_speed=3;
Ki_speed=0.02;
Kd_speed=0.001;

%% гидродинамика 
X_u=0.19527153731335806552340608345245*pi;
Y_v=0.41263768244106697264420608767777*pi;
N_r=0.00042408227451351946151570047859646*pi;
Ma=diag([X_u Y_v N_r]);
%% 

% X direction surge PID

% % PID_surge.Kp=4;
% % PID_surge.Ki=0.0001;
% % PID_surge.Kd=1.2;

% PID_surge.Kp=2;
% PID_surge.Ki=0.01;
% PID_surge.Kd=5;


% PID_surge.Kp=2;
% PID_surge.Ki=0.01;
% PID_surge.Kd=5;
% 
% % Y direction sway PID
% PID_sway.Kp=1.2;
% PID_sway.Ki=0.0125;
% PID_sway.Kd=4;
% 
% % Z rotation yaw PID
% PID_yaw.Kp=7;
% PID_yaw.Ki=0;
% PID_yaw.Kd=0.1;
%% регулятор скорости
% Te=L_phase/R_phase;
% Tu=0.1*Te;



% %% 
% syms r_x r_y f_1 f_2 f_3 f_4 beta positive
% rb1=[r_x;r_y;0];
% rb2=[r_x;-r_y;0];
% rb3=[-r_x;-r_y;0];
% rb4=[-r_x;r_y;0];
% 
% fb1=[f_1*sin(beta);-f_1*cos(beta);0]
% fb2=[f_2*sin(beta);f_2*cos(beta);0]
% fb3=[f_3*sin(beta);-f_3*cos(beta);0]
% fb4=[f_4*sin(beta);f_4*cos(beta);0]
% 
% M1=cross(rb1,fb1)
% M2=cross(rb2,fb2)
% M3=cross(rb3,fb3)
% M4=cross(rb4,fb4)